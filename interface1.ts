interface Rectangle {
    height: number,
    width: number
  }
  
  interface ColoreRectangle extends Rectangle {
    color: string
  }
  const rectangle: Rectangle = {
    height: 20,
    width: 10
  };
  console.log(rectangle)

  const ColoreRectangle: ColoreRectangle = {
    height: 20,
    width: 10,
    color: "red"
  }
  console.log(ColoreRectangle)